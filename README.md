# Stooq reader

### Setup

requirements:
* java
* maven
* nodejs

Install:
```
cd src/main/resources/static/
```
```
npm install
npm run bundle
```
```
cd /stooq-reader:
```
```
mvn clean install
```
### Run

```
docker-compose up
```

Visit http://192.168.99.100:3333/ (your docker machin IP address)

You should see something similiar to this screenshot:
![Alt text](example.png?raw=true "Stooq Reader sample view")