package com.mbadziong.stooq.stooq.report.service;

import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.report.model.ReportRow;
import com.mbadziong.stooq.stooq.report.repository.ReportRepository;
import com.mbadziong.stooq.testdata.SampleMarketIndex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class ReportServiceTest {

    @InjectMocks
    private ReportService reportService;

    @Mock
    private ReportRepository repository;

    @Test
    public void testHandleNewMarketIndex_validMarketIndex() {
        reportService.handleNewMarketIndex(SampleMarketIndex.MARKET_INDEX);

        verify(repository, times(1)).save(new ReportRow(SampleMarketIndex.MARKET_INDEX));
    }

    @Test
    public void testHandleNewMarketIndex_invalidMarketIndex() {
        MarketIndex marketIndex = SampleMarketIndex.MARKET_INDEX_WITH_NULL;

        reportService.handleNewMarketIndex(marketIndex);

        verify(repository, times(0)).save(new ReportRow(SampleMarketIndex.MARKET_INDEX));
    }

    @Test
    public void testHandleNewMarketIndex_twoTimesTheSameMarketIndex() {
        reportService.handleNewMarketIndex(SampleMarketIndex.MARKET_INDEX);
        reportService.handleNewMarketIndex(SampleMarketIndex.MARKET_INDEX);

        verify(repository, times(1)).save(new ReportRow(SampleMarketIndex.MARKET_INDEX));
    }

}