package com.mbadziong.stooq.stooq.data.service;

import com.mbadziong.stooq.stooq.data.marketindex.*;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.data.parser.MarketIndexReadDTO;
import com.mbadziong.stooq.testdata.SampleMarketIndex;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class StooqDataServiceTest {

    @InjectMocks
    private StooqDataService stooqDataService;

    @Mock
    private Wig wig;

    @Mock
    private Wig20 wig20;

    @Mock
    private Wig20Fut wig20Fut;

    @Mock
    private Mwig40 mwig40;

    @Mock
    private Swig80 swig80;

    @Before
    public void setUp() {
        when(wig.getCurrentRead()).thenReturn(SampleMarketIndex.WIG);
        when(wig20.getCurrentRead()).thenReturn(SampleMarketIndex.WIG20);
        when(wig20Fut.getCurrentRead()).thenReturn(SampleMarketIndex.WIG20FUT);
        when(mwig40.getCurrentRead()).thenReturn(SampleMarketIndex.MWIG40);
        when(swig80.getCurrentRead()).thenReturn(SampleMarketIndex.SWIG80);
    }

    @Test
    public void testGetWig() {
        MarketIndexReadDTO wig = stooqDataService.getWig();

        assertEquals(SampleMarketIndex.WIG, wig);
    }

    @Test
    public void testGetWig20() {
        MarketIndexReadDTO wig20 = stooqDataService.getWig20();

        assertEquals(SampleMarketIndex.WIG20, wig20);
    }

    @Test
    public void testGetWig20Fut() {
        MarketIndexReadDTO wig20fut = stooqDataService.getWig20Fut();

        assertEquals(SampleMarketIndex.WIG20FUT, wig20fut);
    }

    @Test
    public void testGetMgiw40() {
        MarketIndexReadDTO mwig40 = stooqDataService.getMgiw40();

        assertEquals(SampleMarketIndex.MWIG40, mwig40);
    }

    @Test
    public void testGetSwig80() {
        MarketIndexReadDTO swig80 = stooqDataService.getSwig80();

        assertEquals(SampleMarketIndex.SWIG80, swig80);
    }

    @Test
    public void testGetAll() {
        MarketIndex current = stooqDataService.getAll();

        assertEquals(SampleMarketIndex.MARKET_INDEX, current);
    }
}