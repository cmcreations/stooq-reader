package com.mbadziong.stooq.testdata;

import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.data.parser.MarketIndexReadDTO;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Date;

public class SampleMarketIndex {
    public static final MarketIndexReadDTO VALID_WIG = new MarketIndexReadDTO(new BigDecimal("59406.36"), new DateTime(new Date(1)));

    public static final MarketIndexReadDTO WIG = new MarketIndexReadDTO(BigDecimal.valueOf(1.11),  new DateTime(new Date(1)));
    public static final MarketIndexReadDTO WIG20 = new MarketIndexReadDTO(BigDecimal.valueOf(2.22), DateTime.now());
    public static final MarketIndexReadDTO WIG20FUT = new MarketIndexReadDTO(BigDecimal.valueOf(3.33), DateTime.now());
    public static final MarketIndexReadDTO MWIG40 = new MarketIndexReadDTO(BigDecimal.valueOf(4.44), DateTime.now());
    public static final MarketIndexReadDTO SWIG80 = new MarketIndexReadDTO(BigDecimal.valueOf(5.55), DateTime.now());

    public static final MarketIndex MARKET_INDEX = new MarketIndex(
            WIG.getValue(),
            WIG20.getValue(),
            WIG20FUT.getValue(),
            MWIG40.getValue(),
            SWIG80.getValue(),
            VALID_WIG.getDateTime()
    );

    public static final MarketIndex MARKET_INDEX_WITH_NULL = new MarketIndex(
            WIG.getValue(),
            WIG20.getValue(),
            null,
            MWIG40.getValue(),
            SWIG80.getValue(),
            VALID_WIG.getDateTime()
    );
}
