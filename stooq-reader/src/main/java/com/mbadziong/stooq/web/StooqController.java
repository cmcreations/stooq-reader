package com.mbadziong.stooq.web;

import com.mbadziong.stooq.stooq.report.model.ReportRow;
import com.mbadziong.stooq.stooq.report.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StooqController {

    @Autowired
    private ReportRepository repository;

    @RequestMapping(value = "/stooq", method = RequestMethod.GET)
    public @ResponseBody List<ReportRow> getStooq() {
        return repository.findAll();
    }

}
