package com.mbadziong.stooq.stooq.data.marketindex;


import com.mbadziong.stooq.stooq.data.exception.CsvFormatException;
import com.mbadziong.stooq.stooq.data.parser.MarketIndexReadDTO;
import com.mbadziong.stooq.stooq.data.model.StooqMarketIndex;
import com.mbadziong.stooq.stooq.data.parser.StooqCsvParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public abstract class StooqMarketIndexDownloader {

    private static final Logger LOGGER = LoggerFactory.getLogger(StooqMarketIndexDownloader.class);

    private final static String STOOQ_URL = "https://stooq.pl/q/l/?s=%s&f=sd2t2ohlc&h&e=csv";

    private RestTemplate restTemplate;

    private StooqCsvParser stooqCsvParser;

    StooqMarketIndex stooqMarketIndex;

    private MarketIndexReadDTO latestRead;

    StooqMarketIndexDownloader(RestTemplate restTemplate, StooqCsvParser stooqCsvParser) {
        this.restTemplate = restTemplate;
        this.stooqCsvParser = stooqCsvParser;
    }

    public MarketIndexReadDTO getCurrentRead() {
        MarketIndexReadDTO currentRead = latestRead;

        try {
            String response = restTemplate.getForObject(
                    String.format(STOOQ_URL, stooqMarketIndex.stockMarketIndex()),
                    String.class);

            currentRead = stooqCsvParser.getMarketIndexRead(response);
            latestRead = currentRead;
        } catch (CsvFormatException e) {
            LOGGER.error("Error during csv parse, returning latest value instead.", e);
        } catch (RestClientException e) {
            LOGGER.error("Error during getting csv, returning latest value instead.", e);
        }

        return currentRead;
    }
}
