package com.mbadziong.stooq.stooq.data.parser;

import org.joda.time.DateTime;

import java.math.BigDecimal;

public class MarketIndexReadDTO {

    private BigDecimal value;

    private DateTime dateTime;

    public MarketIndexReadDTO(BigDecimal value, DateTime dateTime) {
        this.value = value;
        this.dateTime = dateTime;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }
}
