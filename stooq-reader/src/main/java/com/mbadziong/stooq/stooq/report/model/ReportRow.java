package com.mbadziong.stooq.stooq.report.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class ReportRow {

    @Id
    @GeneratedValue()
    @JsonIgnore
    private Long id;

    @NotNull
    private BigDecimal wig;

    @NotNull
    private BigDecimal wig20;

    @NotNull
    private BigDecimal wig20fut;

    @NotNull
    private BigDecimal mwig40;

    @NotNull
    private BigDecimal swig80;

    @NotNull
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonProperty("time")
    private DateTime dateTime;

    public ReportRow(){
    }

    public ReportRow(MarketIndex marketIndex) {
        this.wig = marketIndex.getWig();
        this.wig20 = marketIndex.getWig20();
        this.wig20fut = marketIndex.getWig20fut();
        this.mwig40 = marketIndex.getMwig40();
        this.swig80 = marketIndex.getSwig80();
        this.dateTime = marketIndex.getDateTime();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getWig() {
        return wig;
    }

    public void setWig(BigDecimal wig) {
        this.wig = wig;
    }

    public BigDecimal getWig20() {
        return wig20;
    }

    public void setWig20(BigDecimal wig20) {
        this.wig20 = wig20;
    }

    public BigDecimal getWig20fut() {
        return wig20fut;
    }

    public void setWig20fut(BigDecimal wig20fut) {
        this.wig20fut = wig20fut;
    }

    public BigDecimal getMwig40() {
        return mwig40;
    }

    public void setMwig40(BigDecimal mwig40) {
        this.mwig40 = mwig40;
    }

    public BigDecimal getSwig80() {
        return swig80;
    }

    public void setSwig80(BigDecimal swig80) {
        this.swig80 = swig80;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportRow reportRow = (ReportRow) o;
        return Objects.equals(wig, reportRow.wig) &&
                Objects.equals(wig20, reportRow.wig20) &&
                Objects.equals(wig20fut, reportRow.wig20fut) &&
                Objects.equals(mwig40, reportRow.mwig40) &&
                Objects.equals(swig80, reportRow.swig80) &&
                Objects.equals(dateTime, reportRow.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wig, wig20, wig20fut, mwig40, swig80, dateTime);
    }

    @Override
    public String toString() {
        return "ReportRow{" +
                "wig=" + wig +
                ", wig20=" + wig20 +
                ", wig20fut=" + wig20fut +
                ", mwig40=" + mwig40 +
                ", swig80=" + swig80 +
                ", dateTime=" + dateTime +
                '}';
    }

}
