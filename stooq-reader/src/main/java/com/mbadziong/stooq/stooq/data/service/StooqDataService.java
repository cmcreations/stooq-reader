package com.mbadziong.stooq.stooq.data.service;

import com.mbadziong.stooq.stooq.data.marketindex.Mwig40;
import com.mbadziong.stooq.stooq.data.marketindex.Swig80;
import com.mbadziong.stooq.stooq.data.marketindex.Wig;
import com.mbadziong.stooq.stooq.data.marketindex.Wig20;
import com.mbadziong.stooq.stooq.data.marketindex.Wig20Fut;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.data.parser.MarketIndexReadDTO;
import org.springframework.stereotype.Service;

@Service
public class StooqDataService {

    private Wig wig;

    private Wig20 wig20;

    private Wig20Fut wig20Fut;

    private Mwig40 mwig40;

    private Swig80 swig80;

    public StooqDataService(Wig wig, Wig20 wig20, Wig20Fut wig20Fut, Mwig40 mwig40, Swig80 swig80) {
        this.wig = wig;
        this.wig20 = wig20;
        this.wig20Fut = wig20Fut;
        this.mwig40 = mwig40;
        this.swig80 = swig80;
    }

    public MarketIndexReadDTO getWig() {
        return wig.getCurrentRead();
    }

    public MarketIndexReadDTO getWig20() {
        return wig20.getCurrentRead();
}

    public MarketIndexReadDTO getWig20Fut() {
        return wig20Fut.getCurrentRead();
    }

    public MarketIndexReadDTO getMgiw40() {
        return mwig40.getCurrentRead();
    }

    public MarketIndexReadDTO getSwig80() {
        return swig80.getCurrentRead();
    }

    public MarketIndex getAll() {
        return new MarketIndex(
                getWig().getValue(),
                getWig20().getValue(),
                getWig20Fut().getValue(),
                getMgiw40().getValue(),
                getSwig80().getValue(),
                getWig().getDateTime()
        );
    }
}
