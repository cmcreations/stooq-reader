package com.mbadziong.stooq.stooq.data.parser;

import com.mbadziong.stooq.stooq.data.exception.CsvFormatException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class StooqCsvParser {

    private static final int CSV_DATE_INDEX = 1;

    private static final int CSV_TIME_INDEX = 2;

    private static final int CSV_VALUE_INDEX = 6;

    private static final int STOOQ_CSV_RESPONSE_ROWS = 2;

    private static final int STOOQ_CSV_RESPONSE_COULUMNS = 2;

    private static final String STOOQ_CSV_DELIMITER = ",";

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(DateTimeZone.forID("Europe/Warsaw"));

    private Function<String, MarketIndexReadDTO> CSV_ROW_TO_MARKET_INDEX = (line) -> {
        String[] p = line.split(STOOQ_CSV_DELIMITER);
        DateTime dateTime = DateTime.parse(p[CSV_DATE_INDEX] + " " + p[CSV_TIME_INDEX], DATE_TIME_FORMATTER);
        return new MarketIndexReadDTO(new BigDecimal(p[CSV_VALUE_INDEX]), dateTime);
    };

    public MarketIndexReadDTO getMarketIndexRead(String csv) throws CsvFormatException {
        return parseFromCsv(csv);
    }

    private MarketIndexReadDTO parseFromCsv(String csv) throws CsvFormatException {
        try (BufferedReader reader = new BufferedReader(new StringReader(csv))) {
            List<MarketIndexReadDTO> inputList = reader.lines()
                .skip(1)
                .map(CSV_ROW_TO_MARKET_INDEX)
                .collect(Collectors.toList());
            return inputList.get(0);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new CsvFormatException("Not valid csv. Expected " + STOOQ_CSV_RESPONSE_COULUMNS + " columns");
        } catch (IndexOutOfBoundsException e) {
            throw new CsvFormatException("Not valid stooq csv response. Wanted " + STOOQ_CSV_RESPONSE_ROWS + " rows");
        } catch (IOException e) {
            throw new CsvFormatException("Not valid csv.");
        }
    }

}
