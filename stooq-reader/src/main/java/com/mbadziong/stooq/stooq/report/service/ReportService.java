package com.mbadziong.stooq.stooq.report.service;

import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.report.model.ReportRow;
import com.mbadziong.stooq.stooq.report.repository.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ReportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);

    private ReportRepository repository;

    private ReportRow latest;

    public ReportService(ReportRepository repository) {
        this.repository = repository;
    }

    public Optional<ReportRow> handleNewMarketIndex(MarketIndex marketIndex)  {
        ReportRow reportRow = new ReportRow(marketIndex);
        if ( !reportRow.equals(latest) && !marketIndex.hasNulls() ) {
            repository.save(reportRow);
            latest = reportRow;
            return Optional.of(reportRow);
        }
        LOGGER.info("Skipping storing marketIndex, current values are the same as latest or empty.");
        return Optional.empty();
    }
}
