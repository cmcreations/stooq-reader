package com.mbadziong.stooq.stooq.report.repository;

import com.mbadziong.stooq.stooq.report.model.ReportRow;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<ReportRow, Long> {
}
